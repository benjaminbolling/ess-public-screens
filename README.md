# ESS Public OPI Screens

This repository contains the structure to create the public opi screens running at the address http://pos.esss.lu.se


## Development

If you want to contribute to the screens the sequence is the following:

- Install Docker on your computer and clone the git archive
- Install pre-commit: https://pre-commit.com
- Install the git hook scripts (only required the first time you clone the repo):

  ```
  pre-commit install
  ```

- Build the docker images with
```
docker-compose build
```
- Verify that you can run the current version of the screen with
```
$ docker-compose up
```
- Open your browser and go to http://localhost:8000

To deploy to production (for Emanuele), you should tag and push to GitLab:

```
$ git tag -a <x.x.x>
$ git push --tags
```

The new version of POS comes with a Python API in the backend with these endpoints:

/data/-/health
Returns "OK" if the script is running.

/data/api/v1/caget/{pvlist}
It takes a list of pv separated by comma and returns a JSON dictionary with the properties of each PV.

/data/api/v1/archiver/{pv}
It takes a PV and returns the status of the PV in the archiver (if it is archived or not).

/api/v1/archiver/{pvlist}/{start_time}/{end_time}/{samples}/{output}
It takes a list of PV separated by comma, a starting and ending time in ISO 8601 time in UTC, the number of samples per PV and the output format.
The samples are obtained as a decimation of one every nth data in the dataset. If a PV has N values in the time interval selected and the samples is M, then the program will return 1 value every N/M, not an average in that interval. In other words, the method used to get the data is the "nth" of the Archiver Appliance API (https://slacmshankar.github.io/epicsarchiver_docs/userguide.html). Consider that N/M can be not an integer, so the nearest integer is returned and this means that the total number of samples can differ from what the user specified.
The available output are raw, simple, freeboard. The raw returns the JSON dictionay for every PV produced by the Archiver Appliance, the simple returns a dictionary with two lists for each PV one with values and one with timestamps, the freeboard returns a list of pairs (value, timestamp) used in the POS screen.

/api/v1/archiver/{pvlist}/{seconds}/{samples}/{output}
This endpoint has the same characteristics of the previous one with the difference that the time interval is specified in seconds from the current moment. So if you want to have, for example, the archived values of a PV in the last two days you have to specify 172800 seconds (60seconds x 60minutes x 24hours x 2days).

/data/api/v1/channelfinder/{searchlist}/{output}
It takes a list of words, separated by comma and an output that can be raw or simple.
The raw output returns each PV with a JSON dictionary describing the status of the PV in the Channel Finder, the simple option returns only the list of PVs that match the terms in the searchlist.
