import os
import uvicorn
import epics
from p4p.client.thread import Context
from functools import partial
import time
import requests
import json
from fastapi import FastAPI
from fastapi.responses import PlainTextResponse
import dateutil.parser
import datetime
from epicsarchiver import ArchiverAppliance
from channelfinder import ChannelFinderClient
from concurrent.futures import ThreadPoolExecutor

PORT_NUMBER = 8080
ARCHIVER_SERVER = os.environ.get("ARCHIVER_SERVER")
archiver = ArchiverAppliance(ARCHIVER_SERVER)
CHANNEL_FINDER = os.environ.get("CHANNEL_FINDER")
cf = ChannelFinderClient(BaseURL=CHANNEL_FINDER)

app = FastAPI()

executor = ThreadPoolExecutor(max_workers=20)

epics_ctxt = Context("pva")
monitored_pvs = {}
monitored_epics = {}


def archiver_isalive():
    try:
        url = "http://{}:17668/".format(ARCHIVER_SERVER)
        requests.head(url, timeout=0.3)
        return True
    except requests.exceptions.Timeout:
        return False


def epics_callback(name, value):
    global monitored_pvs
    monitored_pvs[name] = value.raw.todict()
    if hasattr(value, "size") and value.size > 1:
        monitored_pvs[name]["value"] = monitored_pvs[name]["value"].tolist()


@app.get("/api/v2/pvget/{pvlist}")
def get_pvget(pvlist: str):
    global monitored_pvs
    global monitored_epics
    pvnames = pvlist.split(",")
    for pv in pvnames:
        if pv not in monitored_epics:
            monitored_epics[pv] = epics_ctxt.monitor(pv, partial(epics_callback, pv))
    return json.loads(json.dumps(monitored_pvs).replace("NaN", "null"))


@app.get("/api/v2/cleanpvs")
def get_cleanpvs():
    global monitored_pvs
    global monitored_epics
    for pv in monitored_epics:
        monitored_epics[pv].close()
    monitored_pvs.clear()
    monitored_epics.clear()
    return PlainTextResponse("OK")


@app.get("/-/health")
def heath():
    return PlainTextResponse("OK")


@app.get("/api/v1/caget/{pvlist}")
def get_pv(pvlist: str):
    pvnames = pvlist.split(",")
    pvs = [epics.get_pv(pvname, auto_monitor=True) for pvname in pvnames]
    time.sleep(1)
    result = {p.pvname: p._args for p in pvs}
    for pv in pvs:
        if hasattr(result[pv.pvname]["chid"], "value"):
            result[pv.pvname]["chid"] = result[pv.pvname]["chid"].value
        if result[pv.pvname]["char_value"] == "[]":
            result[pv.pvname]["value"] = []
        if (
            result[pv.pvname]["count"] is not None
            and result[pv.pvname]["value"] is not None
            and result[pv.pvname]["count"] > 1
            and type(result[pv.pvname]["value"]) != list
        ):
            result[pv.pvname]["value"] = result[pv.pvname]["value"].tolist()

    return json.loads(json.dumps(result))


@app.get("/api/v1/archiver/{pv}")
def check_archiver(pv: str):
    if archiver_isalive():
        return archiver.get_pv_status(pv=pv)[0]
    else:
        return "Archiver is dead"


def get_archiver(pv: str, start_time: str, end_time: str, samples: int, output: str):
    if not archiver_isalive():
        return "Archiver is dead"
    try:
        dateutil.parser.isoparse(start_time)
    except Exception:
        return (
            "Start Time is not in ISO 8601 format. Example: 2008-09-03T20:56:35.450686Z"
        )
    try:
        dateutil.parser.isoparse(end_time)
    except Exception:
        return (
            "End Time is not in ISO 8601 format. Example: 2008-09-03T20:56:35.450686Z"
        )
    try:
        url = "http://{}:17668/retrieval/data/getData.json?pv=ncount({})&from={}&to={}".format(
            ARCHIVER_SERVER, pv, start_time, end_time
        )
        entries = int(requests.get(url).json()[0]["data"][0]["val"])
        if samples == 0 or entries <= samples:
            reduced_entries = 1
        else:
            reduced_entries = int(entries // samples)

        url = "http://{}:17668/retrieval/data/getData.json?pv=nth_{}({})&from={}&to={}".format(
            ARCHIVER_SERVER, reduced_entries, pv, start_time, end_time
        )
        r = requests.get(url).json()
        if len(r) > 0:
            r = r[0]["data"]
        else:
            r = []
    except Exception:
        return {"Error": "It was not possible to get the data from the archiver."}

    if output == "simple":
        timestamps = []
        values = []
        for entry in r:
            timestamps.append(entry["secs"] + entry["nanos"] / 1e9)
            values.append(entry["val"])
        return {"timestamps": timestamps, "values": values}

    if output == "freeboard":
        timestamps = []
        values = []
        for entry in r:
            timestamps.append(int(entry["secs"] * 1000) + int(entry["nanos"] / 1e6))
            #            values.append(round(entry["val"], 3))
            values.append(entry["val"])
        return list(zip(timestamps, values))
    return r


@app.get("/api/v1/archiver/{pvlist}/{start_time}/{end_time}/{samples}/{output}")
def get_archiver_from_interval(
    pvlist: str, start_time: str, end_time: str, samples: int, output: str
):
    tasks = {}
    for pv in pvlist.split(","):
        tasks[pv] = executor.submit(
            get_archiver, pv, start_time, end_time, samples, output
        )
    r = {}
    for pv in pvlist.split(","):
        r[pv] = tasks[pv].result()
    return r


@app.get("/api/v1/archiver/{pvlist}/{seconds}/{samples}/{output}")
async def get_archiver_from_secs(pvlist: str, seconds: int, samples: int, output: str):
    try:
        start_time = (
            datetime.datetime.now() + datetime.timedelta(seconds=-seconds)
        ).isoformat() + "Z"
        end_time = datetime.datetime.now().isoformat() + "Z"
    except Exception:
        return "Error with time format"

    tasks = {}
    for pv in pvlist.split(","):
        tasks[pv] = executor.submit(
            get_archiver, pv, start_time, end_time, samples, output
        )
    r = {}
    for pv in pvlist.split(","):
        r[pv] = tasks[pv].result()
    return r


@app.get("/api/v1/channelfinder/{searchlist}/{output}")
def search_cf(searchlist: str, output: str):
    wordlist = searchlist.split(",")
    if len(wordlist) <= 1 and len(wordlist[0]) < 2:
        return "Too few worlds or first word shorter than three chars"

    all_casings = (
        lambda s: s
        and {r[0] + t for r in {s, s.swapcase()} for t in all_casings(s[1:])}
        or {s}
    )

    search_string = ""
    if sum(len(i) for i in wordlist) <= 10:
        for word in wordlist:
            for case in all_casings(word):
                search_string += "*" + case + "*|"
            search_string = search_string[:-1]
            search_string += ","
        search_string = search_string[:-1]
    else:
        for word in wordlist:
            search_string += "*" + word + "*,"
        search_string = search_string[:-1]
    try:
        result = cf.find(name=search_string)
        if len(result) == 0:
            return "No PV found."
        else:
            if output == "simple":
                returnlist = []
                for pv in result:
                    returnlist.append(pv["name"])
                return returnlist
            else:
                return result
    except Exception:
        return "Channel Finder Server Not Available."


if __name__ == "__main__":
    uvicorn.run(
        app, host="0.0.0.0", port=PORT_NUMBER,
    )
